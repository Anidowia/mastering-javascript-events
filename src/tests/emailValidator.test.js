import { describe, it } from "mocha";
import { expect } from "chai";
import sinon from "sinon";
import {
  validateAsync,
  validateWithThrow,
  validateWithLog,
  validate,
} from "../javascript/email-validator.js";

describe("Email validator", () => {
  describe("validate function", () => {
    it("Should return false for an empty email", () => {
      expect(validate("")).to.equal(false);
    });
    it("Should return false for an invalid email format", () => {
      expect(validate("email@email")).to.equal(false);
      expect(validate("email@com")).to.equal(false);
    });
    it("Should return true for a valid email with @ and ending", () => {
      expect(validate("dina@gmail.com")).to.equal(true);
    });
  });

  describe("validateAsync", () => {
    it("Should return true asynchronously for a valid email", async () => {
      const result = await validateAsync("email@gmail.com");
      expect(result).to.equal(true);
    });

    it("Should return false asynchronously for an invalid email", async () => {
      try {
        await validateAsync("invalid");
      } catch (error) {
        expect(error.message).to.equal("Invalid email: invalid");
      }
    });
  });
  describe("validateWithThrow", () => {
    it("Should return true for a valid email", () => {
      const result = validateWithThrow("email@gmail.com");
      expect(result).to.equal(true);
    });

    it("Should throw an error for an invalid email", () => {
      expect(() => {
        validateWithThrow("invalid");
      }).to.throw(Error, "Invalid email: invalid");
    });
  });

  describe("validateWithLog", () => {
    it("Should log the email validation result", () => {
      const consoleStub = sinon.stub(console, "log");
      validateWithLog("email@gmail.com");
      expect(consoleStub.calledOnceWith("Email validation result:", true))
        .to.be.true;
      consoleStub.restore();
    });
  });
});
