export async function loadFontsAsync() {
  const font1 = document.createElement("link");
  font1.href =
    "https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;500;600;700&display=swap";
  font1.rel = "stylesheet";

  const font2 = document.createElement("link");
  font2.href =
    "https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,200;0,300;0,400;0,600;0,700;0,900;1,200;1,300;1,400;1,600;1,700;1,900&display=swap";
  font2.rel = "stylesheet";

  document.head.appendChild(font1);
  document.head.appendChild(font2);
}
