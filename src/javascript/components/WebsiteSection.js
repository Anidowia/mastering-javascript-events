export default class WebsiteSection extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
    this.render();
  }

  static get observedAttributes() {
    return ["title", "subtitle", "buttons", "additionalTexts"];
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (oldValue !== newValue) {
      this.render();
    }
  }

  render() {
    const title = this.getAttribute("title") || "";
    const subtitle = this.getAttribute("subtitle") || "";
    const buttons = this.getAttribute("buttons") || "";
    const additionalTexts = this.getAttribute("additionalTexts") || "";

    this.shadowRoot.innerHTML = `
      <style>
        
      body {
        background-color: #fff;
        font: normal 16px "Source Sans Pro", sans-serif, Arial;
      }

      h1,
      h2,
      h3,
      h4,
      h5,
      h6,
      button {
        font-family: "Oswald", sans-serif;
      }

      main {
        background-color: #f9f9f9;
      }

      #app-container {
        max-width: 1200px;
        margin: 0 auto;
        position: relative;
      }

      .app-section {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
      }

      .app-logo {
        width: 64px;
        height: 64px;
        object-fit: contain;
      }

      .app-logo--small {
        width: 20px;
        height: 20px;
        margin-right: 8px;
      }

      .app-title {
        font-size: 3.25rem;
        line-height: 3.2rem;
        text-align: center;
      }

      .app-subtitle {
        font-size: 1.5rem;
        line-height: 1.625rem;
        font-weight: 300;
        text-align: center;
        font-family: "Source Sans Pro", sans-serif, Arial;
      }

      .app-section__button {
        background-color: #55c2d8;
        border: none;
        color: white;
        cursor: pointer;
        outline: none;
      }
      </style>
      <section class="app-section">
      <h2 class="app-title">${title}</h2>
      <h3 class="app-subtitle">${subtitle}</h3>
      <slot></slot>
      <p class="app-additional-texts">${additionalTexts}</p>      
      ${buttons ? `<button class="app-section__button">${buttons}</button>` : ''}
      </section>
    `;
  }
}
