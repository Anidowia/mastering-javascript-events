export async function fetchCommunityData() {
  try {
    const response = await fetch("http://localhost:3000/community");
    if (!response.ok) {
      throw new Error("Failed to fetch data");
    }
    return await response.json();
  } catch (error) {
    console.error(error);
    throw error;
  }
}
export async function fetchSubscribe(input, button, email) {
  try {
    const response = await fetch("http://localhost:3000/subscribe", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    });
    const data = await response.json();
    if (data.success) {
      localStorage.setItem("subscriptionEmail", email);
      input.style.display = "none";
      button.classList.remove("app-section__button--subscribe");
      button.classList.add("app-section__button--unsubscribe");
      button.textContent = "Unsubscribe";
      alert("Subscribed!");
    } else {
      alert(data.error || "Failed to subscribe");
      input.value = "";
    }
  } catch (error) {
    console.error(error);
    alert(error.message || "Failed to subscribe");
  }
}

export async function fetchUnsubscribe(email) {
  try {
    const response = await fetch("http://localhost:3000/unsubscribe", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    });
    if (!response.ok) {
      throw new Error("Failed to unsubscribe");
    }
  } catch (error) {
    console.error(error);
    throw error;
  }
}
