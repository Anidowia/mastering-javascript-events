import "../styles/style.css";
// import "./card-community.js";
// import { SectionCreator } from "./join-us-section.js";
import loadFontsAsync from "./fonts.js";
import WebsiteSection from "./web components/WebsiteSection.js";

customElements.define("website-section", WebsiteSection);

document.addEventListener("DOMContentLoaded", function () {

  loadFontsAsync();

  // function createStandardSection() {
  //   const standardSection = SectionCreator.create("standard");

  //   standardSection.element
  //     .querySelector(".app-section__join-program form")
  //     .addEventListener("submit", function (event) {
  //       event.preventDefault();
  //     });
  // }

  // createStandardSection();
});
