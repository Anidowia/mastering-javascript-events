const VALID_EMAIL_ENDINGS = ["gmail.com", "outlook.com", "yandex.ru"];
export function validate(email) {
  for (const ending of VALID_EMAIL_ENDINGS) {
    if (email.endsWith("@" + ending)) {
      return true;
    }
  }
  return false;
}

export function validateAsync(email) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (validate(email)) {
        resolve(true);
      } else {
        reject(new Error("Invalid email: " + email));
      }
    }, 1000);
  });
}

export function validateWithThrow(email) {
  if (validate(email)) {
    return true;
  } else {
    throw new Error("Invalid email: " + email);
  }
}

export function validateWithLog(email) {
  const isValid = validate(email);
  console.log("Email validation result:", isValid);
  return isValid;
}
