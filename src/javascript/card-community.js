import { fetchCommunityData } from "./api.js";

async function CreateCards() {
  try {
    const communityData = await fetchCommunityData();

    const communitySection = document.createElement("section");
    communitySection.classList.add("app-section__community");

    const titleH2 = document.createElement("h2");
    titleH2.classList.add("app-title");
    titleH2.innerHTML = "Big Community of <br /> People Like You";

    const subtitleH3 = document.createElement("h3");
    subtitleH3.classList.add("app-subtitle");
    subtitleH3.innerHTML = `We’re proud of our products, and we’re really excited
                          <br /> when we get feedback from our users.`;

    const nestedCommunitySection = document.createElement("div");
    nestedCommunitySection.classList.add(
      "app-section__community",
      "app-section__community--cards",
    );

    communitySection.appendChild(titleH2);
    communitySection.appendChild(subtitleH3);

    communitySection.appendChild(nestedCommunitySection);

    communityData.forEach((member, index) => {
      const cardDiv = document.createElement("div");
      cardDiv.classList.add(
        "app-section__community",
        "app-section__community--info",
      );

      const avatarImg = document.createElement("img");
      avatarImg.src = member.avatar;
      avatarImg.alt = `${member.firstName} ${member.lastName}`;
      avatarImg.height = "100";
      avatarImg.width = "100";

      const paragraphBeforeH4 = document.createElement("h3");
      let paragraphText = "";
      switch (index) {
        case 0:
          paragraphText =
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor.";
          break;
        case 1:
          paragraphText =
            "Ut enim ad minim veniam, quis nostrud ullamco laboris nisi ut. Voluptate velit esse cillum dolore eu.";
          break;
        case 2:
          paragraphText =
            "Aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit fugiat nulla pariatur.";
          break;
        default:
          paragraphText = "";
          break;
      }
      paragraphBeforeH4.textContent = paragraphText;

      const nameParagraph = document.createElement("h4");
      nameParagraph.textContent = `${member.firstName} ${member.lastName}`;

      const positionParagraph = document.createElement("p");
      positionParagraph.textContent = member.position;

      cardDiv.appendChild(avatarImg);
      cardDiv.appendChild(paragraphBeforeH4);
      cardDiv.appendChild(nameParagraph);
      cardDiv.appendChild(positionParagraph);

      nestedCommunitySection.appendChild(cardDiv);
    });

    const sectionToInsertBefore =
      document.querySelector("culture-section");
    sectionToInsertBefore.parentNode.insertBefore(
      communitySection,
      sectionToInsertBefore,
    );
  } catch (error) {
    console.error(error);
  }
}
document.addEventListener("DOMContentLoaded", CreateCards);
