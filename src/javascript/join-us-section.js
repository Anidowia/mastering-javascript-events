import { validate } from "./email-validator.js";
import { fetchSubscribe, fetchUnsubscribe } from "./api.js";
class Section {
  constructor(element) {
    this.element = element;
  }

  remove() {
    this.element.remove();
  }

  addTitle(titleText) {
    const title = document.createElement("h1");
    title.classList.add("app-title");
    title.textContent = titleText;
    this.element.appendChild(title);
  }

  addSubtitle(subtitleText) {
    const subtitle = document.createElement("h2");
    subtitle.classList.add("app-subtitle");
    subtitle.innerHTML = subtitleText;
    this.element.appendChild(subtitle);
  }

  addForm(inputPlaceholder, buttonText, buttonClass) {
    const form = document.createElement("form");

    const input = document.createElement("input");
    input.setAttribute("type", "text");
    input.setAttribute("placeholder", inputPlaceholder);
    form.appendChild(input);

    const button = document.createElement("button");
    button.setAttribute("type", "submit");
    button.classList.add("app-section__button", buttonClass);
    button.textContent = buttonText;
    form.appendChild(button);

    const inputGroup = document.createElement("div");
    inputGroup.classList.add("input-group");
    inputGroup.appendChild(form);

    this.element.appendChild(inputGroup);

    return { form, input, button };
  }

  async handleFormSubmit(event, input, button) {
    const email = input.value;
    button.disabled = true;
    button.style.opacity = 0.5;

    try {
      if (button.classList.contains("app-section__button--unsubscribe")) {
        await this.unsubscribe(input, button, email);
        alert("Unsubscribed!");
      } else {
        await fetchSubscribe(input, button, email);
      }
    } catch (error) {
      console.error(error);
      alert(error.message || "Failed to subscribe/unsubscribe");
    } finally {
      button.disabled = false;
      button.style.opacity = 1;
    }
  }

  addFormSubmitListener(form, input, button) {
    const storedEmail = localStorage.getItem("subscriptionEmail");

    if (storedEmail) {
      input.value = storedEmail;
    }

    form.addEventListener("submit", (event) => {
      event.preventDefault();
      const email = input.value;

      if (!validate(email)) {
        alert("Email is not valid!");
        localStorage.removeItem("subscriptionEmail");
        input.value = "";
        return;
      }
      this.handleFormSubmit(event, input, button);
    });
  }

  async unsubscribe(input, button, email) {
    try {
      await fetchUnsubscribe(email);
      localStorage.removeItem("subscriptionEmail");
      input.style.display = "inline-block";
      button.classList.remove("app-section__button--unsubscribe");
      button.classList.add("app-section__button--subscribe");
      button.textContent = "Subscribe";
      input.value = "";
    } catch (error) {
      console.error(error);
      alert("Failed to unsubscribe");
    }
  }
}

export class StandardSection extends Section {
  constructor() {
    super(document.createElement("section"));
    this.element.classList.add("app-section", "app-section__join-program");

    this.addTitle("Join Our Program");
    this.addSubtitle(
      "Sed do eiusmod tempor incididunt <br /> ut labore et dolore magna aliqua.",
    );

    const { form, input, button } = this.addForm(
      "Email",
      "Subscribe",
      "app-section__button--subscribe",
    );
    this.addFormSubmitListener(form, input, button);
  }
}

export class AdvancedSection extends Section {
  constructor() {
    super(document.createElement("section"));
    this.element.classList.add("app-section", "app-section__join-program");

    this.addTitle("Join Our Advanced Program");
    this.addSubtitle(
      "Sed do eiusmod tempor incididunt <br /> ut labore et dolore magna aliqua.",
    );

    const { form, input, button } = this.addForm(
      "Email",
      "Subscribe to Advanced Program",
      "app-section__button--advanced",
    );

    this.addFormSubmitListener(form, input, button);
  }
}

export class SectionCreator {
  static create(type) {
    if (type === "standard") {
      return new StandardSection();
    } else if (type === "advanced") {
      return new AdvancedSection();
    } else {
      throw new Error("Invalid section type");
    }
  }
}
